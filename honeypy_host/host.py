import re
from cerberus import Validator
from copy import deepcopy
from flask import Flask, Response, request
from flask_cors import CORS
from honeypy.common import Common
from honeypy.database import Database
from honeypy_host import api
from flask_basicauth import BasicAuth

from pymongo.errors import DuplicateKeyError

"""
    Allow Cross origin requests while in development
"""
CORS(api, resources={r'\/?.*': {'origins': ['http://localhost:3000', 'http://localhost:4200', "http://frontend-service.default.svc.cluster.local", "http://honeypy-react.default.svc.cluster.local/"]}})

"""
    Instantiate Database, Common and Validator
"""
common = Common()
db = Database(api.config['DATABASE_IP'], api.config['DATABASE_PORT'], api.config['HOST_DB_NAME'], api.config['HOST_DB_COLLECTION'])
validator = Validator({}, purge_unknown = True)

"""
    Define schemas
"""
HOST_CREATE_SCHEMA = {
    "name": {
        "type": "string",
        "required": True,
        "min":1,
        "max":50,
    },
    "kind": {
        "type": "string",
        "allowed": ["host"],
        "default": "host"
    },
    "location": {
        "type": "string",
        "required": True,
        "regex": "^(?:http(s)?:\/\/)?.*$"
    },
    "description": {
        "type": "string",
        "default": "",
        "empty": True
    },
    "active": {
        "type": "boolean",
        "default": True
    }
}

HOST_SAVE_SCHEMA = deepcopy(HOST_CREATE_SCHEMA)
HOST_SAVE_SCHEMA["name"].pop("required", None)
HOST_SAVE_SCHEMA["kind"].pop("default", None)
HOST_SAVE_SCHEMA["location"].pop("required", None)
HOST_SAVE_SCHEMA["description"].pop("default", None)
HOST_SAVE_SCHEMA["active"].pop("default", None)

basic_auth = BasicAuth(api)

@api.route("/", methods = ["POST"])
@basic_auth.required
def create_host():
    """
        Create a host
    """
    validator.schema = HOST_CREATE_SCHEMA
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        try:
            db.insert_one(data)
            return common.create_response(201)
        except DuplicateKeyError as error:
            field = re.search(r'(?:index: (.*)_1)', str(error)).groups()[0]
            value = re.search(r'(?:\{ : "(.*)" \})', str(error)).groups()[0]
            return common.create_response(409, {field: f"'{value}' already exists"})
    else:
        return common.create_response(400, validator.errors)

@api.route("/<name>", methods = ["GET"])
@basic_auth.required
def get_host(name):
    """
        Get a host by name
    """
    document = db.find_one({"name":name})
    if document:
        return common.create_response(200, document)
    else:
        return common.create_response(404, {"name": "Host does not exist"})

@api.route("/<name>", methods = ["PATCH"])
@basic_auth.required
def save_host(name):
    """
        Save a host
    """
    validator.schema = HOST_SAVE_SCHEMA
    data = validator.normalized(request.get_json())
    validation = validator.validate(data)
    if validation == True:
        result = db.update_one({"name":name}, {"$set": data})
        if result.matched_count > 0:
            return common.create_response(204)
        else:
            return common.create_response(404, {"name": "Host does not exist"})
    else:
        return common.create_response(400, validator.errors)

@api.route("/", methods = ["GET"])
@basic_auth.required
def get_hosts():
    """
        Get all hosts as list
    """
    hosts = db.find({})
    return common.create_response(200, hosts)

@api.route("/<name>", methods = ["delete"])
@basic_auth.required
def delete_host(name):
    """
        Delete a host by name
    """
    result = db.delete_one({"name":name})
    if result.deleted_count > 0:
        return common.create_response(204)
    else:
        return common.create_response(404, {"name": "Host does not exist"})

def main():
    db.create_index("name")
    if api.config["PRODUCTION"]:
        api.run(host=api.config["IP"], port=api.config["PORT"], threaded=True)
    else:
        api.run(host=api.config["HOST_IP"], port=api.config["HOST_PORT"], threaded=True)
