import pytest
from copy import deepcopy
from honeypy.api.host import HostService

host_service = HostService()

host = {
    "name": "CRUD TEST",
    "location": "http://0.0.0.0:4444/wd/hub"
}

updated_host = deepcopy(host)
updated_host["name"] = "New Name"
updated_host["location"] = "http://0.0.0.0:4444/asdfdasfsd"
updated_host["active"] = False
updated_host["description"] = "Description Test"

def test_verify_host_deleted():
    """
        DELETE: Verify host does not exist yet
    """
    response = host_service.delete(host["name"])
    response = host_service.delete(updated_host["name"])
    response = host_service.get(host["name"])
    assert response.status_code == 404
    response = host_service.get(updated_host["name"])
    assert response.status_code == 404


def test_host_doesnt_exist():
    """
        GET: Verify host does not exist yet
    """
    response = host_service.get(host["name"])
    assert response.status_code == 404

def test_host_create():
    """
        Create a host

        POST
    """
    response = host_service.create(host)
    assert response.status_code == 201

def test_verify_create():
    """
        GET: Verify the host was created
    """
    response = host_service.get(host["name"])
    assert response.status_code == 200
    data = response.json()
    result = False
    if all (k in data for k,v in host.items()):
        result = True
    assert result

def test_create_duplicate():
    """
        Try creating a duplicate host with the same name
    """
    response = host_service.create(host)
    assert response.status_code == 409

def test_save():
    """
        PATCH: Verify the ability to save/updated a host
    """
    response = host_service.save(host["name"], updated_host)
    assert response.status_code == 204

def test_verify_save():
    """
        GET: Verify the save persists
    """
    response = host_service.get(updated_host["name"])
    assert response.status_code == 200
    data = response.json()
    result = False
    if all (k in data for k,v in updated_host.items()):
        result = True
    assert result

def test_verify_old_host():
    """
        GET: Verify old host name no longer exists after the save
    """
    response = host_service.get(host["name"])
    assert response.status_code == 404

def test_delete_host():
    """
        DELETE: Delete the host
        DELETE: Verify it was deleted
    """
    response = host_service.delete(updated_host["name"])
    assert response.status_code == 204
    response = host_service.delete(updated_host["name"])
    assert response.status_code == 404

def test_get_host():
    """
        GET: Verify the host was deleted
    """
    response = host_service.get(updated_host["name"])
    assert response.status_code == 404
