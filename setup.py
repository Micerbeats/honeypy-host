from setuptools import setup

setup(
    name = "honeypy_host",
    version = "0.1",
    description = "Honeypy Host Service",
    url = "https://bitbucket.org/Micerbeats/honeypy-host",
    author = "Micah Prescott",
    author_email = "prescottmicah@gmail.com",
    packages = [
        "honeypy_host",
        "honeypy_host.tests",
        "honeypy_host.configs"
    ],
    install_requires = [
        "Cerberus",
        "Flask",
        "Flask-Cors",
        "Flask-BasicAuth",
        "pymongo",
        "requests"
    ],
    classifiers = [
        "'Programming Language :: Python :: 3.6'"
    ],
    entry_points = {
        "console_scripts": [
            "honeypy_host = honeypy_host.host:main"
        ]
    }
)
